package shpp.com.example.app;


import shpp.com.example.abstraction.ChatMediator;
import shpp.com.example.abstraction.UserAbstract;
import shpp.com.example.concrete.TextChat;
import shpp.com.example.concrete.User;

public class AppMediator {
    public static void main(String[] args) {
        ChatMediator chat = new TextChat();
        UserAbstract user1 = new User(chat, "Pankaj");
        UserAbstract user2 = new User(chat, "Lisa");
        UserAbstract user3 = new User(chat, "Saurabh");
        UserAbstract user4 = new User(chat, "David");
        chat.addUser(user1);
        chat.addUser(user2);
        chat.addUser(user3);
        chat.addUser(user4);
        user1.send("Hi All");
        user2.send("Hello!");
    }
}
