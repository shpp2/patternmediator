package shpp.com.example.concrete;

import shpp.com.example.abstraction.ChatMediator;
import shpp.com.example.abstraction.UserAbstract;

import java.util.ArrayList;
import java.util.List;

// імпелементація ChatMediator, тобо посередника

/**
 * Даний клас створює список користувачів і є посередником між ними, тобто
 * додоє користувача в чат (список), і відправляє месседжи цім користувачам.
 */
public class TextChat implements ChatMediator {
    private final List<UserAbstract> user;

    public TextChat() {
        this.user = new ArrayList<>();
    }

    public void sendMessage(String message, UserAbstract user) {
        for (UserAbstract value : this.user) {
            //message should not be received by the user sending it
            if (value != user) {
                user.receive(message);
            }
        }
    }

    public void addUser(UserAbstract user) {
        this.user.add(user);
    }
}
