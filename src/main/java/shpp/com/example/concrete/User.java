package shpp.com.example.concrete;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import shpp.com.example.abstraction.ChatMediator;
import shpp.com.example.abstraction.UserAbstract;

public class User extends UserAbstract {
    private final Logger logger = LoggerFactory.getLogger(User.class);

    public User(ChatMediator mediator, String name) {
        super(mediator, name);
    }

    public void send(String message) {
            logger.info("{} sending message {}", this.name, message);
        mediator.sendMessage(message, this);
    }

    public void receive(String message) {
        logger.info("{} receive message {}", this.name, message);
    }
}
