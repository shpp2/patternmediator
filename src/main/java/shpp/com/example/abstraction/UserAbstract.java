package shpp.com.example.abstraction;

public abstract class UserAbstract {
    protected ChatMediator mediator;
    protected String name;

    public UserAbstract(ChatMediator mediator, String name) {
        this.mediator = mediator;
        this.name = name;
    }
    public abstract void send(String message);
    public abstract void receive(String message);
}
