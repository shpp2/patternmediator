package shpp.com.example.abstraction;

public interface ChatMediator {

    public void sendMessage(String message, UserAbstract user);

    public void addUser(UserAbstract user);
}
